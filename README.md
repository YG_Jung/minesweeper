#개발 스택

### 프론트엔드
* React
* Redux
* Styled Component
* Babel
* Webpack

### 백엔드
* Express
* MongoDB
* Mongoose


작성일자: 2020-01-11
