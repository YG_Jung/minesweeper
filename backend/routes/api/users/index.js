import express from 'express';

import User from '../../../model/user';

const router = express.Router();

router.get('/', async (req, res, next) => {
  const { level = 'Beginner' } = req.query;
  try {
    res.json(
      await User.find({ level })
      .sort({ elapsed: 1 })
      .limit(10)
    );
  } catch (e) {
    next(e);
  }
});

router.post('/create', async (req, res, next) => {
  try {
    res.json(await User.create({ ...req.body }));
  } catch (e) {
    next(e);
  }
});

export default router;
