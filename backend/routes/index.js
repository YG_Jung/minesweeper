import express from 'express';

import users from './api/users';

const router = express.Router();

router.use('/api/users', users);

export default router;
