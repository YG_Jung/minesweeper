import mongoose from 'mongoose';

const { Schema } = mongoose;

const User = new Schema({
  name: {
    type: String,
    required: true,
  },
  elapsed: Number,
  level: {
    type: String,
    index: true,
  },
}, {
  timestamps: {
    createdAt: true,
    updatedAt: false,
  },
});

export default mongoose.model('User', User, 'user');
