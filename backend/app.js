import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';

import routes from './routes';
import config from './config';

const app = express();

const mongoUri = `${config.mongoHost}/${config.mongodb.name}`;

app.use(cors({
  origin(origin, callback) {
    return callback(null, true);
  },
  optionsSuccessStatus: 200,
}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

mongoose.connect(mongoUri, config.mongodb.options).catch((error) => {
  console.log(`Failed to connect to MongoDB: ${error}`);
});

app.use('/', routes);

app.listen(config.mongoServerPort, (err) => {
  console.log(`Server started running on port: ${config.mongoServerPort}`)
  if (err) {
    console.log(err);
  }
});
