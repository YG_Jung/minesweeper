import common from './env/common';
import local from './env/local';
import prod from './env/prod';

const env = process.env.NODE_ENV || 'local';
const configs = { common, prod, local };

export default {
  ...common,
  ...configs[env],
};
