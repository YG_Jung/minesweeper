export default {
  mongoApihost: 'http://localhost:1501',
  mongoServerPort: 1501,
  mongoHost: 'mongodb://localhost:27017',
  mongodb: {
    name: 'minesweeper',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
};
