import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const propTypes = {
  src: PropTypes.string,
};

const defaultProps = {
};

// ====

const StyledImage = styled.img`
  max-width: 100%;
`;

const Image = ({
  src,
  props,
}) => (
  <StyledImage
    src={src}
    {...props}
  />
);

Image.propTypes = propTypes;
Image.defaultProps = defaultProps;

export default Image;
