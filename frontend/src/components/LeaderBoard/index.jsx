import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { getUsers } from 'actions/users';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  users: PropTypes.array.isRequired,
  errors: PropTypes.object,
  level: PropTypes.string,
};

const defaultProps = {
  errors: {},
};

// ====

const Root = styled.div``;

const SmallText = styled.span`
  font-size: 10px;
`;

const Table = styled.table`
  &, & td {
    border: 1px solid #ccc;
  }
`;

const LeaderBoard = ({
  dispatch,
  isFetching,
  isError,
  users,
  errors,
  level,
}) => {
  const getDateStr = (date) => {
    const d = new Date(date);
    return [d.getYear() - 100, d.getMonth() + 1, d.getDate()].join('-');
  };

  useEffect(() => {
    dispatch(getUsers(level));
  }, [level])

  return (
    <Root>
      {
        isFetching
          ? 'Loading...'
          : (
            <Table>
              <tr>
                <th>
                  <SmallText>
                    Rank
                  </SmallText>
                </th>
                <th>
                  <SmallText>
                    Name
                  </SmallText>
                </th>
                <th>
                  <SmallText>
                    Level
                  </SmallText>
                </th>
                <th>
                  <SmallText>
                    Time
                  </SmallText>
                </th>
                <th>
                  <SmallText>
                    Date
                  </SmallText>
                </th>
              </tr>
              {
                users?.map(({ name, elapsed, level, createdAt }, i) => (
                  <tr key={i}>
                    <td>
                      {i + 1}
                    </td>
                    <td>
                      <SmallText>
                        {name}
                      </SmallText>
                    </td>
                    <td>
                      <SmallText>
                        {level}
                      </SmallText>
                    </td>
                    <td>
                      {`${elapsed}s`}
                    </td>
                    <td>
                      <SmallText>
                        {getDateStr(createdAt)}
                      </SmallText>
                    </td>
                  </tr>
                ))
              }
            </Table>
          )
      }
    </Root>
  );
};

LeaderBoard.propTypes = propTypes;
LeaderBoard.defaultProps = defaultProps;

export default connect(({ users }) => {
  return { ...users };
})(LeaderBoard);
