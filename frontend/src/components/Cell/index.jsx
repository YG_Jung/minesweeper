import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Card from 'components/Card';
import Image from 'components/Image';

import {
  checkMatrix,
  openCell,
  setGameOver,
  toggleFlag,
} from 'actions/canvas';

import flag from './flag.png';
import mine from './mine.png';

const propsTypes = {
  coordinates: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  hasFlag: PropTypes.bool,
  isFreeze: PropTypes.bool,
  isMine: PropTypes.bool,
  isOpen: PropTypes.bool,
  nearMinesCount: PropTypes.number,
};

const defaultProps = {
};

// ====

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 20px;
  height: 20px;
  &:hover {
    cursor: pointer;
  }
`;

const Closed = styled.div`
  width: 100%;
  height: 100%;
  background-color: #ccc;
`;

const Opened = styled.div`
`;

const Cell = ({
  coordinates,
  dispatch,
  hasFlag,
  isFreeze,
  isMine,
  isOpen,
  nearMinesCount,
}) => {
  const handleCellClick = (e) => {
    e.preventDefault();
    if (isFreeze) {
      return;
    }
    if (e.nativeEvent?.which === 1 && !hasFlag) {
      dispatch(openCell({ coordinates }));
    }
    if (e.nativeEvent?.which === 3 && !isOpen) {
      dispatch(toggleFlag({ coordinates }));
      dispatch(checkMatrix());
    }
  };

  useEffect(() => {
    if (!isFreeze && isOpen && isMine) {
      dispatch(setGameOver());
    }
  }, [isOpen]);

  return (
    <Root
      onClick={(e) => handleCellClick(e)}
      onContextMenu={(e) => handleCellClick(e)}
    >
      {
        isOpen
          ? (
            <Opened>
              {
                isMine
                  ? <Image src={mine} />
                  : nearMinesCount > 0 ? nearMinesCount : ''
              }
            </Opened>
          )
          : (
            <Closed>
              {
                hasFlag
                  ? <Image src={flag} />
                  : ''
              }
            </Closed>
          )
      }
    </Root>
  );
}

export default connect(({ canvas }, { coordinates }) => {
  const { matrix, isGameOver, isGameClear } = canvas;
  const [x, y] = coordinates.split(',');
  return {
    isFreeze: (isGameOver || isGameClear),
    ...matrix[x][y],
  };
})(Cell);
