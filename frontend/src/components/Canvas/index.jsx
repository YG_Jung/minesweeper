import { times } from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import { Margin, Padding, mt, mr } from 'styled-components-spacing';

import Card from 'components/Card';
import Cell from 'components/Cell';
import LeaderBoard from 'components/LeaderBoard';

import { initCanvas, setGameClear } from 'actions/canvas';
import { addUser, getUsers } from 'actions/users';
import { shuffleMinesToMatrix } from 'utils';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  mineCount: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  isGameClear: PropTypes.bool,
  isGameOver: PropTypes.bool,
  matrix: PropTypes.array,
  name: PropTypes.string,
};

const defaultProps = {
  isGameClear: false,
  isGameOver: false,
  matrix: [],
};

// ====

const Root = styled.div``;

const Center = styled.div`
  text-align: center;
`;

const StyledCard = styled(Card)`
`;

const StyledButton = styled.button`
  background-color: skyblue;
`;

const StyledGrid = styled(Grid)`
  margin-right: -0.25rem;
  & ${Grid.Unit} > div + div {
    ${mt(1)};
    ${mr(1)};
  }
`;

const Text = styled.h3`
  text-align: center;
  color: red;
  margin: 0;
`;

const Table = styled.table`
  margin: auto;
  &, & td {
    border: 1px solid #ccc;
  }
`;

const gameSizeData = [
  {
    name: 'Beginner',
    description: '8 x 8',
    mineCount: 6,
    value: 8,
  },
  {
    name: 'Intermediate',
    description: '14 x 14',
    mineCount: 22,
    value: 14,
  },
  {
    name: 'Expert',
    description: '20 x 20',
    mineCount: 50,
    value: 20,
  },
];

const Canvas = ({
  dispatch,
  isGameClear,
  isGameOver,
  matrix,
  mineCount,
  remainingFlagCount,
  name,
  size,
}) => {
  const [canvasInfo, setCanvasInfo] = useState({
    name: 'Beginner',
    size: 8,
    mineCount: 6,
    remainingFlagCount: 6,
    matrix: [],
  });
  const [timeElapsed, setTimeElapsed] = useState(0);
  const [isTimerRunning, setIsTimerRunning] = useState(true);
  const [userName, setUserName] = useState('');

  const handleGameOptionChange = (name, size, mineCount) => {
    setCanvasInfo({ name, size, mineCount });
  }

  const handleUserSubmit = () => {
    dispatch(addUser({
      level: name,
      name: userName,
      elapsed: timeElapsed,
    }));
  };

  const handleSubmit = () => {
    const matrix = shuffleMinesToMatrix(size, size, mineCount);
    dispatch(initCanvas({
      ...canvasInfo,
      remainingFlagCount: mineCount,
      matrix,
    }));
    setTimeElapsed(0);
    setIsTimerRunning(true);
  };

  const useInterval = (cb, delay) => {
    const callback = useRef();

    useEffect(() => {
      callback.current = cb;
    }, [cb])

    useEffect(() => {
      const tick = () => {
        callback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  };

  useInterval(() => {
    setTimeElapsed(timeElapsed + 1);
  }, isTimerRunning ? 1000 : null);

  useEffect(() => {
    if (mineCount) {
      handleSubmit();
    }
  }, [size]);

  useEffect(() => {
    if (isGameClear) {
      dispatch(setGameClear({ timeElapsed }));
      setIsTimerRunning(false);
    }
  }, [isGameClear]);

  return (
    <Root onContextMenu={(e) => e.preventDefault()}>
      <Center>
        <h1>
          Minesweeper!
        </h1>
      </Center>
      <Table>
        <tbody>
          <tr>
            <th>Level</th>
            <th>Size</th>
            <th>Mines</th>
          </tr>
          {
            gameSizeData.map(({ name, description, mineCount, value }, i) => (
              <tr key={i}>
                <td>
                  <input
                    type="radio"
                    id={value}
                    name="gameSize"
                    value={value}
                    onClick={() => handleGameOptionChange(name, value, mineCount)}
                    checked={value == canvasInfo.size}
                  />
                  <label for={value}>{name}</label>
                </td>
                <td>{description}</td>
                <td>{mineCount}</td>
              </tr>
            ))
          }
        </tbody>
      </Table>
      <Center>
        <Padding vertical={3}>
          <StyledButton onClick={() => handleSubmit()}>
            {
              (isGameClear || isGameOver)
                ? 'Try Again'
                : 'New Game'
            }
          </StyledButton>
        </Padding>
      </Center>

      {
        isGameOver
          ? <Text>Game Over!</Text>
          : isGameClear
            ? (
              <React.Fragment>
                <Text>Awesome!</Text>
                <input
                  type="text"
                  placeholder="Type your name!"
                  value={userName}
                  onChange={(e) => setUserName(e.target.value)}
                />
                <button onClick={() => handleUserSubmit()}>
                  Submit
                </button>
              </React.Fragment>
            )
            : ''
      }

      <Padding vertical={3}>
        <Grid>
          <Grid.Unit size={1 / 2}>
            <Card sectioned>
              <span>
                Flag:
              </span>
              {` ${remainingFlagCount || 0}`}
            </Card>
          </Grid.Unit>
          <Grid.Unit size={1 / 2}>
            <Card sectioned>
              <span>
                Timer:
              </span>
              {` ${timeElapsed}`}
            </Card>
          </Grid.Unit>
        </Grid>
        <Margin top={2}>
          <StyledCard sectioned>
            <StyledGrid>
              {
                matrix.map((row, i) => (
                  <Grid.Unit key={i} size={1 / row.length}>
                    {
                      row.map((col, j) => (
                        <Cell
                          key={j}
                          coordinates={`${i},${j}`}
                        />
                      ))
                    }
                  </Grid.Unit>
                ))
              }
            </StyledGrid>
          </StyledCard>
        </Margin>
      </Padding>
      <LeaderBoard level={name} />
    </Root>
  );
}

Canvas.propTypes = propTypes;
Canvas.defaultProps = defaultProps;

export default connect(({ canvas }) => {
  return { ...canvas };
})(Canvas);
