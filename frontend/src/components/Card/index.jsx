import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { p } from 'styled-components-spacing';

const propTypes = {
  sectioned: PropTypes.bool,
};

const defaultProps = {
  sectioned: false,
};

const Card = styled.div`
  ${({ sectioned }) => (sectioned ? p(1) : null)};
  border: 1px solid #ccc;
  border-radius: 2px;
`;

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

export default Card;
