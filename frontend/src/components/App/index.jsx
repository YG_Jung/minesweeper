import React from 'react';
import styled from 'styled-components';

import Canvas from 'components/Canvas';

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
`;

const App = () => (
  <Root>
    <Canvas />
  </Root>
);

export default App;
