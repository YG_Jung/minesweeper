import config from '../../config';

export const get = (url='', options={}) => {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'GET',
      ...options,
      headers: {
        ...options?.headers,
        ...config.defaultApiHeaders,
      },
    }).then(response => {
      if (response.ok) {
        response.json().then((res) => {
          resolve(res);
        });
      } else {
        response.json().then((res) => {
          reject(res);
        });
      }
    });
  });
};

export const post = (url='', data={}, options={}) => {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      ...options,
      headers: {
        ...options?.headers,
        ...config.defaultApiHeaders,
      },
    }).then(response => {
      if (response.ok) {
        response.json().then((res) => {
          resolve(res);
        });
      } else {
        response.json().then((res) => {
          reject(res);
        });
      }
    });
  });
};

export const fetchUsers = (query) => {
  return new Promise((resolve, reject) => {
    get(`${config.apiHost}/api/users?level=${query}`)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const createUser = (data) => {
  return new Promise((resolve, reject) => {
    post(`${config.apiHost}/api/users/create`, data)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
