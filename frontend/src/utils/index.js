import { fill, flattenDeep, random, sum, times } from 'lodash';

export const initMatrix = (col, row) => {
  const matrix = [];
  times(col).forEach(i => (
    matrix.push(fill(times(row), 0))
  ));
  return matrix;
};

export const calculateNearMinesCount = (matrix) => {
  const newMatrix = initMatrix(matrix.length, matrix[0].length);
  matrix.map((col, i) => (
    col.map((row, j) => {
      let nearMinesCount = 0;
      [-1, 0, 1].forEach(dx => {
        if (i + dx < 0 || i + dx > col.length - 1) {
          return;
        }
        [-1, 0, 1].forEach(dy => {
          if (j + dy < 0 || j + dy > col.length - 1) {
            return;
          }
          nearMinesCount += matrix[i + dx][j + dy];
        })
      });
      newMatrix[i][j] = {
        isMine: !!matrix[i][j],
        nearMinesCount,
      };
    })
  ));
  return newMatrix;
};

export const shuffleMinesToMatrix = (col, row, mineCount) => {
  const matrix = initMatrix(col, row);

  const colCount = matrix.length;
  const rowCount = matrix[0].length;

  while (sum(flattenDeep(matrix)) < mineCount) {
    const randCol = random(0, colCount - 1);
    const randRow = random(0, rowCount - 1);

    if (!matrix[randRow][randCol]) {
      matrix[randRow][randCol] = 1;
    }
  }
  return calculateNearMinesCount(matrix);
};

export const compareMinesAndFlags = (matrix) => {
  const mineCells = flattenDeep(matrix).filter(({ isMine }) => isMine);
  const flagCells = mineCells.filter(({ hasFlag }) => hasFlag);
  return mineCells.length === flagCells.length;
};

export const openUpMatrix = (col, row, matrix) => {
  times(col).forEach(x => (
    times(row).forEach(y => {
      matrix[x][y].isOpen = true;
    })
  ));
  return matrix;
};

export default {
  calculateNearMinesCount,
  compareMinesAndFlags,
  initMatrix,
  openUpMatrix,
  shuffleMinesToMatrix,
};
