import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from 'components/App';

import configureStore from './redux/store';
import GlobalStyle from './styles';

const store = configureStore();

ReactDOM.render(
  <div>
    <GlobalStyle/>
    <Provider store={store}>
      <App />
    </Provider>
  </div>,
  document.getElementById('root'),
);
