import {
  USERS_REQUEST,
  USERS_SUCCESS,
  USERS_FAILURE,

  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILURE,
} from '../constants';

const initialState = {
  isFetching: false,
  isError: false,
  errors: {},
  users: [],
  user: {},
};

const users = (state = initialState, action) => {
  const { type, payload } = action;
  console.log(action);
  switch (type) {
    case USERS_REQUEST: {
      return {
        ...state,
        isFetching: true,
        isError: false,
      };
    }
    case USERS_SUCCESS: {
      return {
        ...state,
        users: payload,
        isFetching: false,
        isError: false,
        errors: {},
      };
    }
    case USERS_FAILURE: {
      return {
        ...state,
        errors: payload,
        isFetching: false,
        isError: true,
      };
    }
    case CREATE_USER_REQUEST: {
      return {
        ...state,
        isFetching: true,
      };
    }
    case CREATE_USER_SUCCESS: {
      return {
        ...state,
        user: payload,
        isFetching: false,
        isError: false,
        errors: {},
      };
    }
    case CREATE_USER_FAILURE: {
      return {
        ...state,
        errors: payload,
        isFetching: false,
        isError: true,
      };
    }
    default:
      return state;
  }
};

export default users;
