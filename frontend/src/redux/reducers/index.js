import { combineReducers } from 'redux';

import canvas from './canvas';
import users from './users';

export default combineReducers({ canvas, users });
