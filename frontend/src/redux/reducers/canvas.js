import { times } from 'lodash';
import {
  INIT_CANVAS,
  OPEN_CELL,
  TOGGLE_FLAG,
  CHECK_MATRIX,
  SET_GAMEOVER,
  SET_GAMECLEAR,
} from '../constants';

import {
  compareMinesAndFlags,
  openUpMatrix,
} from 'utils';

const initialState = {
  remainingFlagCount: 6,
  isGameClear: false,
  isGameOver: false,
  mineCount: 6,
  matrix: [],
  size: 8,
};

const canvas = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case INIT_CANVAS: {
      return { ...payload };
    }
    case OPEN_CELL: {
      const { matrix } = state;
      const { coordinates } = payload;

      const [x, y] = coordinates.split(',');
      matrix[x][y].isOpen = true;

      return { ...state };
    }
    case TOGGLE_FLAG: {
      const { matrix, mineCount } = state;
      const { coordinates } = payload;

      if (state.remainingFlagCount <= mineCount) {
        const [x, y] = coordinates.split(',');
        const nextFlagState = !matrix[x][y].hasFlag;
        if (state.remainingFlagCount) {
          if (nextFlagState) {
            state.remainingFlagCount -= 1;
            matrix[x][y].hasFlag = nextFlagState;
          } else {
            state.remainingFlagCount += 1;
            matrix[x][y].hasFlag = nextFlagState;
          }
        } else {
          if (!nextFlagState) {
            state.remainingFlagCount += 1;
            matrix[x][y].hasFlag = nextFlagState;
          }
        }
      }

      return {
        ...state,
        remainingFlagCount: state.remainingFlagCount,
      };
    }
    case CHECK_MATRIX: {
      const { matrix } = state;
      return {
        ...state,
        isGameClear: compareMinesAndFlags(matrix),
      };
    }
    case SET_GAMEOVER: {
      const { matrix, size } = state;
      const openedMatrix = openUpMatrix(size, size, matrix);
      return {
        ...state,
        matrix: openedMatrix,
        isGameClear: false,
        isGameOver: true,
      };
    }
    case SET_GAMECLEAR: {
      const { matrix, size } = state;
      const openedMatrix = openUpMatrix(size, size, matrix);
      return {
        ...state,
        matrix: openedMatrix,
        isGameClear: true,
        isGameOver: false,
      };
    }
    default:
      return state;
  }
};

export default canvas;
