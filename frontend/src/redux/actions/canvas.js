import {
  INIT_CANVAS,
  OPEN_CELL,
  TOGGLE_FLAG,
  CHECK_MATRIX,
  SET_GAMEOVER,
  SET_GAMECLEAR,
} from '../constants';

import {
  createUser,
  fetchUsers,
} from 'services';

export const initCanvas = payload => ({
  type: INIT_CANVAS,
  payload,
});

export const openCell = payload => ({
  type: OPEN_CELL,
  payload,
});

export const toggleFlag = payload => ({
  type: TOGGLE_FLAG,
  payload,
});

export const checkMatrix = payload => ({
  type: CHECK_MATRIX,
  payload,
});

export const setGameOver = () => ({
  type: SET_GAMEOVER,
});

export const setGameClear = payload => ({
  type: SET_GAMECLEAR,
  payload,
});
