import {
  USERS_REQUEST,
  USERS_SUCCESS,
  USERS_FAILURE,

  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILURE,
} from '../constants';

import {
  createUser,
  fetchUsers,
} from 'services';

export const usersRequest = payload => ({
  type: USERS_REQUEST,
  payload,
});

export const usersSuccess = payload => ({
  type: USERS_SUCCESS,
  payload,
});

export const usersFailure = payload => ({
  type: USERS_FAILURE,
  payload,
});

export const createUserRequest = payload => ({
  type: CREATE_USER_REQUEST,
  payload,
});

export const createUserSuccess = payload => ({
  type: CREATE_USER_SUCCESS,
  payload,
});

export const createUserFailure = payload => ({
  type: CREATE_USER_FAILURE,
  payload,
});

export const addUser = (payload) => {
  return (dispatch) => {
    dispatch(createUserRequest(payload));
    return createUser(payload)
      .then((response) => {
        dispatch(createUserSuccess(response));
      })
      .catch((response) => {
        dispatch(createUserFailure(response));
      });
  };
};

export const getUsers = (payload) => {
  return (dispatch) => {
    dispatch(usersRequest(payload));
    return fetchUsers(payload)
      .then((response) => {
        dispatch(usersSuccess(response));
      })
      .catch((response) => {
        dispatch(usersFailure(response));
      });
  };
};
