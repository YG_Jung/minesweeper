import {
  applyMiddleware,
  createStore,
} from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducers from './reducers';

const logger = createLogger();

const configureStore = (initialState) => {
  return createStore(
    rootReducers,
    initialState,
    applyMiddleware(thunk, logger),
  );
};

export default configureStore;
