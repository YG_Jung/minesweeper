module.exports = shipit => {
  shipit.initConfig({
    prod: { servers: 'ubuntu@13.209.15.169' },
  });

  shipit.task('deploy', () => {
    const options = { cwd: '/home/ubuntu/minesweeper/frontend' };
    shipit.remote('git pull && npm ci && npm run build', options);
  });
};
