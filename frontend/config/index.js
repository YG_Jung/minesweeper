const mode = process.env.NODE_ENV === 'production' ? 'prod' : 'local';

const config = {
  local: {
    apiHost: 'http://localhost:1501',
  },
  prod: {
    apiHost: 'http://ec2-13-209-15-169.ap-northeast-2.compute.amazonaws.com',
  },
}[mode];

export default {
  ...config,

  defaultApiHeaders: {
    'Content-Type': 'application/json; charset=utf-8',
    'Accept-Language': 'ko-KR',
  },
};
