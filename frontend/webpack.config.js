const path = require('path');

const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

const mode = process.argv.includes('--prod') ? 'prod' : 'dev';
const isDebug = mode === 'dev';
const env = isDebug ? 'development' : 'production';

const config = {
  mode: env,
  name: 'index',
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    compress: true,
    hot: true,
    inline: false,
    contentBase: 'dist',
    port: '1500',
    proxy: {
      '/api': 'http://localhost:1501',
    },
    historyApiFallback: true,
  },
  resolve: {
    extensions: ['.js', '.jsx', 'json'],
    alias: {
      actions: path.resolve(__dirname, './src/redux/actions'),
      components: path.resolve(__dirname, './src/components'),
      services: path.resolve(__dirname, './src/services'),
      utils: path.resolve(__dirname, './src/utils'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
            ],
            plugins: [
              "@babel/plugin-transform-runtime",
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-transform-async-to-generator",
              "@babel/plugin-proposal-optional-chaining",
            ],
          },
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(bmp|gif|jpg|jpeg|png|svg|eot|ijmap|svg|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: false,
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: false,
      template: path.resolve(__dirname, 'public/index.html'),
      minify: isDebug ? null : {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
        favicon: 'public/favicon.ico',
      },
    }),
    new CompressionPlugin({
      test: /\.(js|css|html)$/,
      cache: true,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env),
    }),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        cache: true,
        test: /\.(js|jsx)$/,
      }),
    ],
  },
};

module.exports = config;
